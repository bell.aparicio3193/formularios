<?php
  session_start();  

 /* $varsesion = $_SESSION['num_cta'];
  
  if ($varsesion==null || $varsesion=='') {
    echo 'Usted no tiene autorización.';
    die();
  }*/
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- Bootstrap core CSS --> 
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <!-- CSS -->
  <link rel="stylesheet" href="css/main.css">
  <title>Información</title>
</head>
<body class="bg-light">
  <div class="container">
    <nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-primary">
      <a class="navbar-brand mr-auto mr-lg-0 active" href="info.php">Home</a>
      <button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="formulario.php">Registrar Alumno</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="login.php" id="">Cerrar Sesión</a>
          </li>
        </ul>
      </div>
    </nav>

    <main role="main"> 
      <h1>Usuario Autenticado</h1>
      <div class="card">
        <div class="card-header">
          
         
        <!-- Trabajando con SESIONES -->
        <h2>Hola <?php if(!empty($_SESSION['nombre'])){echo $_SESSION['nombre'];} ?>
        </h2>
        </div>
        <div class="card-body">
          <h4 class="card-title">Información</h4> 
          <!-- Coloque metodo POST porque con SESSION no cargaba el formulario -->
          <p class="card-text">Número de Cuenta: <?php if(isset($_POST['num_cta'])){echo $_POST['num_cta'];} ?>
          </p>
          <p class="card-text">Fecha de Nacimiento: <?php if(isset($_POST['fec_nac'])){echo $_POST['fec_nac'];} ?>
          </p>
        </div>
      </div>
      <div class="tablaInformacion">
        <h2>Datos guardados:</h2> 
        <br>         
        <table class="table table-hover">
          <thead>
            <tr>
              <th>#</th>
              <th>Nombre</th>
              <th>Fecha de Nacimiento</th>
            </tr>
          </thead>
          <tbody>
            <tr>              
              <td>
                <?php if(isset($_POST['num_cta'])){echo $_POST['num_cta'];} ?>
              </td>
              <td>
                <?php if(isset($_POST['nombre'],$_POST['primer_apellido'],$_POST['segundo_apellido'] )){echo $_POST['primer_apellido'], " ", $_POST['segundo_apellido'], " ",$_POST['nombre'];} ?>
              </td>
              <td>
                <?php if(isset($_POST['fec_nac'])){echo $_POST['fec_nac'];} ?>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </main>          
  </div>
  <!-- Destruye -->
  <?php 
    /*unset($_SESSION['num_cta']);
    session_destroy();*/
	?>
  <script src="https://code.jquery.com/jquery-3.1.0.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>