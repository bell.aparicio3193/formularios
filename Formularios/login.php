<?php
  session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- Bootstrap core CSS --> 
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <!-- CSS -->
  <link rel="stylesheet" href="css/main.css">
  <title>Login</title>
</head>
<body class="bg-light">
  <div class="container">
    <main>
      <h1>Login</h1>
      <form action="info.php" method="POST">
        <div>
          <div class="mb-2">
            <label for="">Número de cuenta</label>
            <input type="number" name="num_cta" class="form-control">
          </div>
          <br>
          <div  class="mb-2">
            <label for="">Contraseña</label>
            <input type="password" name="contrasena" class="form-control">
          </div>
          <div  class="mb-2">
            <input type="submit" class="btn btn-primary" value="Enviar">
          </div>
        </div>
      </form>
    </main>
  </div>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>