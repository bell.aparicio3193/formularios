<?php
  session_start();
  
  if(isset($_POST['submit'])){
    // Variables
    $num_cta = $_POST["num_cta"];
    $nombre = $_POST['nombre'];
    $primer_apellido = $_POST['primer_apellido'];
    $segundo_apellido = $_POST['segundo_apellido'];
    $genero = $_POST['genero'];
    $fec_nac = $_POST['fec_nac'];
    $contrasena = $_POST['contrasena'];
  }

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- Bootstrap core CSS --> 
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <!-- CSS -->
  <link rel="stylesheet" href="css/main.css">
  <title>Registrar Alumno</title>
</head>
<body class="bg-light">
  <div class="container">
    <nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-primary">
      <a class="navbar-brand mr-auto mr-lg-0" href="info.php">HOME</a>
      <button class="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="navbar-collapse offcanvas-collapse" id="">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link active" href="formulario.php">Registrar Alumnos</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="login.php" id="">Cerrar Sesión</a>
          </li>
        </ul>
      </div>
    </nav>

    <main>
      <form action="info.php?accion=get&texto=textoenget" method="POST">
        <div>
          <div class="mb-3">
            <label for="">Número de cuenta:</label>
            <input type="number" name="num_cta" id="num_cta" class="form-control" placeholder="Número de cuenta">
          </div>
          <div class="mb-3">
            <label for="">Nombre:</label>
            <input type="text" name="nombre" id="nombre" class="form-control" placeholder="Nombre">
          </div>
          <div class="mb-3">
            <label for="">Primer Apellido:</label>
            <input type="text" name="primer_apellido" id="primer_apellido" class="form-control" placeholder="Primer Apellido">
          </div>
          <div class="mb-3 form-group">
            <label for="">Segundo Apellido:</label>
            <input type="text" name="segundo_apellido" id="segundo_apellido" class="form-control" placeholder="Segundo Apellido">
          </div>
          <div class="form-group">
           <!-- form radio control -->
            <label class="form-label">Genero: </label>
            <div>
              <label class="form-radio">
                <input type="radio" name="genero" value="H" checked>
                <i class="form-icon"></i> Hombre
              </label>
            </div>
            <div>
              <label class="form-radio">
                <input type="radio" name="genero" value="M">
                <i class="form-icon"></i> Mujer
              </label>
            </div>
            <div>
              <label class="form-radio">
                <input type="radio" name="genero" value="O">
                <i class="form-icon"></i> Otro
              </label>
            </div>
          </div>
          <div class="mb-3 form-group">
            <label for="">Fecha de Nacimiento:</label>
            <input type="date" name="fec_nac" id="fec_nac" class="form-control" placeholder="Fecha de Nacimiento">
          </div>
          <div class="mb-3">
            <label for="">Contraseña</label>
            <input type="password" name="contrasena" id="contrasena" class="form-control" placeholder="Contraseña">
          </div>
          <div class="mb-2">
            <input type="submit" name="submit" class="btn btn-primary" value="Registrar">
          </div>
        </div>

        <!-- Validando campos -->
        <?php
          if(isset($_POST['submit'])){
            //comprueba si esta vacio un campo
            if(empty($num_cta)){
              echo"<p class='error'>Agrega tu número de cuenta </p>";
            }
            if(empty($nombre)){
              echo"<p class='error'>Agrega tu nombre </p>";
            }
            if(empty($primer_apellido)){
              echo"<p class='error'>Agrega tu primer Apellido </p>";
            }
            if(empty($segundo_apellido)){
              echo"<p class='error'>Agrega tu segundo apellido </p>";
            }
            if(empty($genero)){
              echo"<p class='error'>Agrega tu nombre </p>";
            }
            if(empty($fec_nac)){
              echo"<p class='error'>Agrega tu fecha de nacimiento </p>";
            }
            if(empty($contrasena)){
              echo"<p class='error'>Agrega tu contraseña </p>";
            }
          }
        ?>
      </form>
    </main>
  </div>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>